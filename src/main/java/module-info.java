module libre.wsdl4j {
    exports javax.wsdl;
    exports javax.wsdl.xml;
    exports javax.wsdl.extensions.http;
    exports javax.wsdl.extensions.mime;
    exports javax.wsdl.extensions.schema;
    exports javax.wsdl.extensions.soap;
    exports javax.wsdl.extensions.soap12;
    exports javax.wsdl.factory;

    requires jakarta.xml.bind;
}